# Partiels SANTE 💖

Les liens avec des ⭐ sont les liens conseiller pour réviser.

## Planning 📅

### Examen Juin *(semaine du 26/06/2023)* 👀

| Mardi                                                                                                   | Horaires      | Documentation | Moodle |
|:--------------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [MPRO](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MPRO/MPRO.md) - ROUSSENQ/GUELIN | 10h30 - 11h30 | ❓            | ✔️     |
| [BINF](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/BINF/BINF.md) - PARUTTO         | 14h - 15h30   | ❓            | ✔️     |
| [ISDM](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/ISDM/ISDM.md) - DANIEL          | 15h45 - 16h45 | ❓            | ✔️     |

| Mercredi                                                                                           | Horaires      | Documentation | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [IHPC](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IHPC/IHPC.md) - DEMICHEL   | 10h30 - 12h   | ❓            | ✔️     |

---

### Plus tard ? 💩

| ?                                                                                                  | Horaires      | Documentation | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [ELDM](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/ELDM/ELDM.md) - Dr RICHARD | ❓            | ❓            | ✔️     |
| [IMED](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IMED/IMED.md) - GERMAIN    | ❓            | ❓            | ✔️     |

---

### Mid partiels *(semaine du 24/04/2023)* 😴

| Lundi                                                                                           | Horaires     | Docu. + Calculatrice | Moodle |
|:------------------------------------------------------------------------------------------------|:------------:|:--------------------:|:------:|
| [PBS2](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/PBS2.md) - GROSCOT | 14h - 15h30  | ✔️                  | ✔️     |
| [PBS2](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/PBS2.md) - HEMON   | 15h45 - 17h15| ✔️                  | ✔️     |

| Mardi                                                                                                  | Horaires     | Documentation | Moodle |
|:-------------------------------------------------------------------------------------------------------|:------------:|:-------------:|:------:|
| [DBRE](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DBRE/DBRE.md) - MOIN           | 9h15 - 10h45 | ✔️           | ✔️     |
| [COIN](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COIN/COIN.md) (MANC) - DEWILDE | 11h - 12h30  | ❓            | ✔️     |

| Mercredi                                                                                             | Horaires     | Documentation | Moodle |
|:-----------------------------------------------------------------------------------------------------|:------------:|:-------------:|:------:|
| [IBINF](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/IBINF.md) - CRUZEL    | 9h30 - 10h30 | ❓            | ✔️     |
| [SYSBP](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/SYSBP/SYSBP.md) - CHABRERIE | 10h45 - 12h15| ❓            | ✔️     |
