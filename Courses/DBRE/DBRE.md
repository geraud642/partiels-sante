# Droit des propriétés intellectuelles ⚖

## Infos 💖
Encore un exam ez, car d'après le fameux mail du 14/04 reçu à 15h40, on auras accès à [ce document](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DBRE/ressources/COURS%20PI%20Christelle%20SASSARD_Marie%20Moin_2023.pdf) sur Moodle.  
Donc perso, ca va juste spam *ctrl. + f* dans le doc comme un gogole et c'est game 👍.

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1198)  
[Doc. qu'on auras le jour de l'examen](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DBRE/ressources/COURS%20PI%20Christelle%20SASSARD_Marie%20Moin_2023.pdf) ⭐  
[Cours pour devenir intelligent](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DBRE/ressources/DBRE.pdf) *(vaut mieux réviser les math je pense)*
